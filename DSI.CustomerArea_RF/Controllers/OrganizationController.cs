﻿namespace DSI.CustomerArea_RF.Controllers
{
    using DSI.CustomerArea_RF.BL;
    using DSI.CustomerArea_RF.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Web;
    using System.Web.Http;

    [RoutePrefix("api/organizations")]
    public class OrganizationController : ApiController
    {
        private BusinessManager businessManager;

        public OrganizationController()
        {
            this.businessManager = new BusinessManager();
        }

        [HttpGet, Route("{id}")]
        public IHttpActionResult GetOrganization(int id)
        {
            Organization organization = this.businessManager.GetOrganization(id);
            return this.Content(HttpStatusCode.OK, organization) ;
        }
    }
}