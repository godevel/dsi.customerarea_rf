﻿namespace DSI.CustomerArea_RF.DL
{
    using GDV.JiraApi.Model;
    using GDV.JiraApi.Service;

    public class DataManager
    {
        public OrganizationInfo GetOrganization(int id)
        {
            OrganizationService organizationService = new OrganizationService();
            return organizationService.GetOrganization(id);
        }
    }
}