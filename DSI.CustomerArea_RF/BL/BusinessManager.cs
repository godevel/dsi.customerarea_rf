﻿using DSI.CustomerArea_RF.DL;
using DSI.CustomerArea_RF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSI.CustomerArea_RF.BL
{
    public class BusinessManager
    {
        private DataManager dataManager;
        public BusinessManager()
        {
            this.dataManager = new DataManager();
        }

        public Organization GetOrganization(int id)
        {
            return (Organization) this.dataManager.GetOrganization(id);
        }
    }
}